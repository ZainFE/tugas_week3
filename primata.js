class Primata {
    constructor(name, age, livingPlace) {
    this.name = name
    this.age = age
    this.livingPlace = livingPlace
    }
    // kita sekarang sedang belajar tentang OOP dan sekarng saya mengibaratkan dengan primata, 
    // primata ini mempunyai objek yang di asumsikan objeknya dengan name, age, dan livePlaca.
    set changeName(newName) {
    this.name = newName
    }
    // primata ini ingin mengganti objek namanya dengan cara setter (set)
    
    get changedName(){
    return `Nama saya sekarang adalah ${this.name}`
    }
    // dan primata ini juga ingin mengambil namanya dengan cara getter (get)
   
}
const monyet = new Primata("Tarzan",1,"Jungle")
    
    console.log(monyet)

    monyet.changeName = "Tutu"

console.log(monyet.changedName)
// setelah rencana selesai, baru di terapkan atau di aplikasikan dengan cara memanggil semuanya itu seperti
// primata (nama,age dan tempat tinggal) dengan cara const.
